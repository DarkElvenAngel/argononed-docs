---
title: "Unix Sockets"
weight: 2
---

Version 0.5.0 introduces a Unix Socket interface.  This interface is different from the shared memory interface in that it uses the **ARC** client short for ARgon Client.  This interface has finer control and more functions.  The daemon limits the number of connections to 3 max.

{{% notice style="warning" %}}
At the time of this writing the **ARC** interface is incomplete
{{% /notice %}}