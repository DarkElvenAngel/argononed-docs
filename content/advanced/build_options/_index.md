---
title: "Build Options"
weight: 1
---

Before preforming a build you may want to set some build flags for you setup.

## Logging Options

The default build will generate a very detailed logs if you want less logging then add  
```make LOGLEVEL=[0-6]```  
The log levels go in this order: NONE, FATAL, CRITICAL, ERROR, WARNING, INFO, DEBUG. A value of 0 disables logging. It is possible to turn logging back on or change the level using the `-l#` command line argument.

## Advanced Build Options

 Advanced Build options are used with `configure` or `package.sh`. Each of these options has a flag counterpart that can be set in config.txt, the configuration file, or a command line argument.

{{% notice style="note" %}}
 
 *Once these flags are set and compiled in there is no way to unset them unless you recompile.*

{{% /notice %}}

 **DISABLE_POWERBUTTON** *or* **FLAG 0x01** if you don't have `/dev/gpiochip#` or you don't want to use the power button then use this flag.  Remember that the Force shutdown >= 5 second long press will still work.

 **RUN_IN_FOREGROUND** *or* **FLAG 0x02** if you need the daemon to always run in the foreground this flag will skip the forking to the background and cause the daemon to log to the console.

 **USE_SYSFS_TEMP** *or* **FLAG 0x04** If your system doesn't have `/dev/vcio` you'll need to use the sysfs temperature sensor set.

## Auto configure {{% badge  style="note" title="Version" %}}0.5.0 {{% /badge %}}

Auto configure will preform checks each time the daemon starts to detect the correct i²c bus, where to read temperature form, and what communication protocol to use.
