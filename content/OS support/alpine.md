---
title: "How to build for Alpine Linux"
hidden: true
---


## Status

Argon ONE {{% badge %}}V1{{% /badge %}}{{% badge %}}V2{{% /badge %}}{{% badge %}}V3{{% /badge %}} {{% badge style="red" icon="exclamation-circle" %}}**INCOMPATIBLE**{{% /badge %}}

Argon EON {{% badge style="red" icon="exclamation-circle" %}}**INCOMPATIBLE**{{% /badge %}}

Argon ARTIK hat {{% badge icon="flask" style="orange" %}}**EXPERIMENTAL**{{% /badge %}}

### Argon ARTIK hat

The Daemon is fully working with this setup the steps below document how to setup for install.

### Argon ONE and EON Cases

There is no way known to detect a *power off* vs *reboot* on this OS without hacks.  Such changes to the system aren't recommended as they may provide challenges to undo or break *with* or *in* future OS updates.

Disabling the power button will not help here, the case needs to be sent a command only when a shutdown is requested without this command the case will continue to power the Pi even though it has powered off.

## Install Steps

{{% notice style="info" %}}
***This isn't a guild to setup ALPINE LINUX***  
{{% /notice %}}

Setup the build environment install the required packages.  

```bash
apk add gcc dtc git argp-standalone bash build-base linux-headers
```  
clone the repo and follow the build steps.
```bash
git clone https://gitlab.com/DarkElvenAngel/argononed.git
./configure
make all install
```

## Work Around

{{% notice style="warning" %}}
**THIS IS NOT RECOMMEND BUT I'M SUPPLYING THIS SOLUTION TO USE AT YOUR OWN RISK**  
With the warning out of the way here is the hack.
{{% /notice %}}

Remove **/sbin/reboot** and replace it with this script

```bash
#!/bin/sh
touch /tmp/reboot
busybox reboot
```

You need to set the execution bit. Next copy the **OS/alpine/argononed.stop** script to **/etc/local.d**. Last you need to activate the script `rc-update add local`.  Now the hack is complete.
