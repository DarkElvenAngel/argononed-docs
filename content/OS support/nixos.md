---
title: "Argon ONE Daemon on NixOS"
hidden: true
---

{{% notice style="tip" %}}
This OS is only supported with versions 0.3.4 and 0.4.0

The patches required to build for NixOS are version dependant.
{{% /notice %}}

## Support Status

### Supported Devices
- Raspberry Pi {{% badge %}}4B{{% /badge %}}
  : Argon ONE {{% badge %}}V2{{% /badge %}}, {{% badge %}}V2 M.2{{% /badge %}}
  : Argon ARTIK Fan HAT

### Untested Devices  

{{% notice style="tip" %}}
It probably works but I couldn't test it

Someone please test it and maybe gives some feedback
{{% /notice %}}

- Raspberry Pi {{% badge %}}3B+{{% /badge %}}
  : Argon ONE case
- Raspberry Pi {{% badge %}}4B{{% /badge %}}
  : Argon ONE {{% badge %}}V1{{% /badge %}}
  : Argon EON
- Argon ARTIK Fan HAT (on other Pi's)


## Installation

### Non-Flake configurations
Find yourselves a way to merge this snippet into your own `configuration.nix` (a new module also suffice):
```nix
{ lib, pkgs, config, ...}:
{
  imports = let
    argononed = fetchGit {
      url = "https://gitlab.com/DarkElvenAngel/argononed.git";
      ref = "master"; # Or any other branches deemed suitable
    };
  in
    [ "${argononed}/OS/nixos" ];

  services.argonone = {
    enable = true;
    logLevel = 4;
    settings = {
      fanTemp0 = 36; fanSpeed0 = 10;
      fanTemp1 = 41; fanSpeed1 = 50;
      fanTemp2 = 46; fanSpeed2 = 80;
      hysteresis = 4;
    };
  };
}
```

And done!

### Flake configurations
Sorry but this repo won't be available as a flake, but this repo doesn't have any external dependencies so it doesn't hurt your config's purity. Alternatively try this:

In `flake.nix`, add:
```nix
{
  inputs.argononed = {
    url = "gitlab:ykis-0-0/argononed/feat/nixos";
    flake = false;
  };
}
```

In a new module (or `configuraion.nix`):
```nix
{ argononed, ...}:
{
  imports = [ "${argononed}/OS/nixos" ];

  services.argonone = {
    enable = true;
    logLevel = 4;
    settings = {
      fanTemp0 = 36; fanSpeed0 = 10;
      fanTemp1 = 41; fanSpeed1 = 50;
      fanTemp2 = 46; fanSpeed2 = 80;
      hysteresis = 4;
    };
  };
}
```

That's all!
Bye!

No I'm just kidding.
## To-Do List (If you want to help)
* `services.logrotate.extraConfig` have been deprecated and going to be removed in NixOS 22.11 ☹ We need to find a way to adapt this.

## Down the rabbit hole(?)
[Welcome.](./SPOILER.md)
