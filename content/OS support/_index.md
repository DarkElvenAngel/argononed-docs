+++
archetype = "chapter"
weight = 20
title = "OS Support"
+++

## Full working

* AlmaLinux Thanks to @ArclightMat
* DietPI
* Fedora-IoT Thanks to @jwillikers for working through
* [Fedora-Server]({{% relref "fedora-server.md" %}}) Thanks to @mjackdk and @ItzMiracleOwO {{% badge style="tip" %}}**See Link for troubleshooting**{{% /badge %}}
* Kali Linux
* Gentoo
* Manjaro-arm
* opensuse Thanks to @fridrich
* OSMC
* [piCore]({{% relref "picore.md" %}}) Thank to @irkode
* Pop OS
* Raspberry Pi OS 32bit or 64bit
* RetroPi
* TwisterOS
* Ubuntu
* Void Linux

## Supported with {{% badge style="blue" icon="box-open" %}}Packager Only{{% /badge %}}

* Lakka
* LibreElec
* [OpenWRT]({{% relref "openwrt.md" %}}) {{% badge icon="flask" style="orange" %}}**EXPERIMENTAL**{{% /badge %}} 

## Experimental Supported OS {{% badge icon="flask" style="orange" %}}**EXPERIMENTAL**{{% /badge %}}

* opensuse-microos {{% badge icon="flask" style="orange" %}}**EXPERIMENTAL**{{% /badge %}}
* [OpenWRT]({{% relref "openwrt.md" %}}) {{% badge icon="flask" style="orange" %}}**EXPERIMENTAL**{{% /badge %}} 

## Additional steps or problematic

The following OS' work but have some quirks and it is advisable to read there support pages for details on how to use this project with these OS'.

* [Alpine Linux]({{% relref "alpine.md" %}}) 
* Arch Linux arm {{% badge style="tip" %}}(ARMv7 installation ONLY){{% /badge %}}
* [NixOS]({{% relref "nixos.md" %}}) Thanks to @ykis-0-0 for all the hard work required for this one. {{% badge style="green" title="Supported with" %}}**0.4.0 ONLY**{{% /badge %}} 
* [Vanilla Debian](OS/debian/README.md) 

## Not listed

If your OS is not listed there is a chance it will still work, go through this list of suggestions.

* Is my OS based off of a supported one?
    - You can use the `TARGET_DISTRO` build flag to override auto detection.
* Can I just try to install?
    - You could however you might put files where they don't belong or just make a mess. Maybe don't do this unless your sure.
* Couldn't I just add support for my OS?
    - You can do that see [the guide]({{% relref "os-support/guide/" %}}) 



