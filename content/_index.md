# Argon One Daemon

A replacement daemon for the Argon One, EON Raspberry Pi cases, and the Argon Artik Fan Hats.

Current stable version is {{% badge  style="green" title="Version" %}}0.4.1{{% /badge %}} 
Current testing version is {{% badge  style="primary" color="#808"  icon="cogs" title="Version" %}}0.5.0{{% /badge %}}

## What's new in 0.4.x

* More control: multiple ways to configure.
* Full rewrite of the base code.

## What's new in 0.5.x

* Support for Argon ONE {{% badge %}}V3{{% /badge %}} and Pi {{% badge %}}5{{% /badge %}}
* Support for Argon IR REMOTE.
* More robust IPC support.
* New cli tool `argonctl`
* More code shuffling
* New argument processor

{{% notice style="warning" title="branch is under active development" %}}

Not all features are working, present, and/or stable. Functions and instruction subject to change without notice.  Please check the compiled code works before you install it.

{{% /notice %}}

## Why make this?

Simply put I didn't like the OEM software.  It works sure but it uses Python and needs to install a bunch of dependencies.  This makes it's foot print on your system much bigger than it needs to be.  My daemon runs with minimal requirements, and requires no extra libraries.

## OS Support

The current list of supported OS's can be found [here]({{% relref "OS support" %}})   

## How To Install

Install instruction are now found [here]({{% relref "install" %}})

## Configuration

Configuration documentation has moved to it's own pages [here]({{% relref "howto" %}})


## Upgrading to the latest version

In order to upgrade to the latest version the current method is to pull the updates from gitlab and execute the following command

```bash
./install
```

## Logging Options

The default build will generate a very detailed logs if you want less logging then add  
```make LOGLEVEL=[0-6]```  
The log levels go in this order: NONE, FATAL, CRITICAL, ERROR, WARNING, INFO, DEBUG. A value of 0 disables logging. It is possible to turn logging back on or change the level using the `-l#` command line argument.


## Fan Modes

The Daemon has 4 modes of operation but will always starts in Automatic mode.  Change modes using the GUI Applet is available or cli tool.

### Cool Down Mode

In cool down mode the fan has a set temperature you want to reach before switching back to automatic control.  This is all set as follows   ```argonone-cli --cooldown <TEMP> [--fan <SPEED>]```  
***NOTE***: *The speed is optional and the default is 10% it's also import to note that if the temperature continues to climb the schedules set for the fan are ignored.*  

### Manual Mode  

As the name implies your in control over the fan the schedules are ignored.  To access this as follows ```argonone-cli --manual [--fan <SPEED>]```  
***NOTE***: *The fan speed is optional and if not set the fans speed is left alone.*

### Auto Mode

This is the default mode the daemon always starts in this mode and will follow the schedules in the setting.  If you want to change to automatic you do so as follows ```argonone-cli --auto```

### Off Mode

Yes an off switch, maybe you want to do something and you need to be sure the fan doesn't turn on and spoil it.  You can turn off the fan as follows ```argonone-cli --off```
***NOTE***: *When the fan is off nothing but turning to a different mode will turn it back on*

## argonone-cli tool

The `argonone-cli` command line tool lets you change setting on the fly. It communicates with shared memory of the daemon, so the daemon must be running for this tool to be of use.

## Setting schedules

Want to adjust the when the fan comes on, maybe it's not staying on long enough you can change all set points in the schedules from the command line **without** rebooting.  the values are fan[0-2] temp[0-2] and hysteresis.  It's important when changing these values that you remember that the daemon will reject bad values and/or change them to something else.  It's also important to commit the changes you make otherwise they won't do anything.  The value rules are simple each stage must to greater than the one before it and there are minimum and max values.  
For temperature the minimum value is 30° the maximum is currently undefined.  
For the fan the minimum speed is 10% and the maximum is 100%.  
For Hysteresis the minimum is 0° and the maximum is 10°  

You can set your values like in this example.  

```bash
argonone-cli --fan0 25 --temp0 50 --hysteresis 10 --commit
```  

The following method is not supported.

```toml
argonone-cli --fan0 25
argonone-cli --temp0 50
argonone-cli --hysteresis 10
argonone-cli --commit
```

The changes **MUST** in one shot and have `--commit` them for them to take effect.

## New error flags  {{% badge %}}0.5.x and up ONLY{{% /badge %}}

New error flags are being added to share memory and will soon be available in a memory decode. The goal of these flags is to make determining the source of an error more obvious without the need to dive through logs that may be disabled depending on your build options.

```text
>> DECODEING MEMORY <<
Fan Status OFF Speed 0%
System Temperature 45°
Hysteresis set to 10°
Fan Speeds set to 10% 55% 100%
Fan Temps set to 55° 60° 65°
Fan Mode [ AUTO ] 
Fan Speed Override 0% 
Target Temperature 0° 
Daemon Status : Waiting for request
Maximum Temperature : 45°
Minimum Temperature : 44°
Daemon Warnings : 0
Daemon Errors : 0
Daemon Critical Errors : 3
```

This is the current output of `argonone-cli --decode` you can see something is wrong but what it is we have to look into the logs.

```text
>> DECODEING MEMORY <<
Fan Status OFF Speed 0%
System Temperature 45°
Hysteresis set to 10°
Fan Speeds set to 10% 55% 100%
Fan Temps set to 55° 60° 65°
Fan Mode [ AUTO ] 
Fan Speed Override 0% 
Target Temperature 0° 
Daemon Status : Waiting for request
Maximum Temperature : 45°
Minimum Temperature : 44°
Daemon Warnings : 0
Daemon Errors : 0
Daemon Critical Errors : 3
Error Flag(s) set
    i2c bus
```

With the flags you can see the error is with the i2c bus.  Once `argonctl` is complete you will be able to diagnose and possible fix problems like this without restarting.

## The new argonctl  {{% badge %}}0.5.x and up ONLY{{% /badge %}}

The `argonctl` tool was meant to replace the `argonone-cli`, `argonctl` will do everything `argonone-cli` can do and more.  Since this tool is in the planning stage this can all change.

The argonone daemon is moving away from the shared memory method of interacting and controlling things.  The daemon will instead use sockets this will allow for two way communications and some of these new features.

The proposed features are

* Changing the schedule or mode *much like the current tool.*
* Loading or saving the configurations
* Monitor mode get real time data from the daemon
* Logging client no matter the loglevel of the daemon you can use the logging client to read log events.
* send commands example restart i2c
* read status the is much like `argonone-cli --decode`
* IR controls the argon one V2 has a built-in IR sensor that can be programed
* Fan control **without** the daemon!!  send fan command directly to the controller with or without the daemon.
