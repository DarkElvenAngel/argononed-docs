---
title: "Overlay Configuration"
weight: 1
---

Configuration starts in the **config.txt** look for this line ```dtoverlay=argonone``` The parameters are simple.

{{% notice style="note" %}}
The location of **config.txt** is dependant on your OS and you should refer to the OS' documentation to know how to access it.
{{% /notice %}}

* **fantemp[0-2]** - Sets the temperatures at which the fan will spin up
* **fanspeed[0-2]** - Sets the speed at which the fan will spin
* **hysteresis** - Sets the hysteresis

The default values are the same as the OEM at 55℃ the fan will start at 10%, at 60℃ the speed will increase to 55% and finally after 65℃ the fan will spin at 100%.  The default hysteresis is 3℃

Version 0.1.0 of the overlay has additional settings

* **argonconf** - Set the location of argononed.conf *default /etc/argononed.conf*
* **argon-i2c** - Set the i2c bus used for the controller *default 1*
* **argon-flag** - This is used to set build flags *default 0* ** See Advanced Build Options for values*

This overlay version also presets the GPIO 4 as and input with it's pull down resistor enabled.

Version 0.2.0 of the overlay has additional settings

* **argon-type** - This is used to set the controller type default 0 ** See controller type list for valid values.

## Example config.txt

In this example the hysteresis will be set to 5 and the fan will start at 50℃

```toml { lineNos="true" wrap="true" title="config.txt" }
[pi4]
dtoverlay=argonone,hysteresis=5
dtparam=fantemp0=50
```
{{% notice style="warning" %}}
`config.txt` has a line length limit of **80** Characters.

It's recommend to use `dtparam` to avoid overflow.

Please refer to the official documentation form Raspberry Pi found [here](https://www.raspberrypi.com/documentation/computers/config_txt.html)
{{% /notice %}}