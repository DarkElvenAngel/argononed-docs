---
title: "Using the Command line"
weight: 3
---

The daemon now accepts command line arguments.

```text
      --fan0=VALUE           Set Fan1 value
      --fan1=VALUE           Set Fan2 value
      --fan2=VALUE           Set Fan3 value
      --fans=VALUE           Set Fan values
      --hysteresis=VALUE     Set Hysteresis
      --temp0=VALUE          Set Temperature1 value
      --temp1=VALUE          Set Temperature2 value
      --temp2=VALUE          Set Temperature3 value
      --temps=VALUE          Set Temperature values
      --conf=FILENAME        load config
      --forceflag=VALUE      Force flags to VALUE
  -F, --forground            Run in Forground
  -l, --loglevel=VALUE       Set Log level
  -c, --colour               Run in Forground with colour
      --dumpconf             Dump build config
  -?, --help                 Give this help list
      --usage                Give a short usage message
  -V, --version              Print program version
```

## Example command line

`# argononed -cl6 --conf=/tmp/argononed.conf` This will start the in foreground with colour output, set the log level to 6 *DEBUG* and set the configuration file to **/tmp/argononed.conf**
