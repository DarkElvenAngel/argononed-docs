---
title: "The Configuration File"
weight: 2
---

The default configuration file can be placed at `/etc/argononed.conf` you can customize this location with `argonconf=FILENAME` in config.txt or with the `--conf=FILENAME` argument set to **argononed**.  The following are the properties and the accepted values.

* fans - set all fan speeds at once each value is comma separated.
* fan*X* - set specific fan speed where *X* is a number from 0 - 2.
* temps - set all temperature set points at once each value is comma separated.
* temp*X* - set specific temperature set point where *X* is a number from 0 - 2.
* hysteresis - set the hysteresis values 0 - 10 are valid
* loglevel - change the log level of the daemon see Logging Options for values.
* i2cbus - change the i2c bus address default is 1 and this value is rarely changed.
* flags - see Advanced Build Options for values. **IMPORTANT this value is in HEX format**
* type - set the micro controller type default is 0 [auto detect] {{% badge %}}0.5.x and up ONLY{{% /badge %}}

## Example configuration file

```toml { lineNos="true" wrap="true" title="/etc/argononed.conf" }
# argononed configuration
fans = 50, 75, 100
temps = 50, 60, 65
hysteresis = 10

flags = 0x04
```