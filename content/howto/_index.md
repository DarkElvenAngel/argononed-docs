+++
archetype = "chapter"
title="How To Configure"
weight=5
+++

There are more ways to configure the daemon than before this is useful for advanced users however general users only need worry about using the [config.txt]({{% relref "config_txt.md" %}}).

Using multiple configuration methods gives the greatest range of flexibility to this software.  The load order is important, the primary configuration comes from the overlay settings these are loaded first and have the lowest priority, next the configuration file is loaded, lastly the command line arguments are read they have the highest priority.  Values are overwritten from lowest to highest priority with one acceptation the flags value.  Flags are added together only the argument `--force-flag-reset` can set the flags to the default value _this is set at compile time_.

What happens if there is no configuration file? The daemon will check if the configuration file exists if it's not found it's noted in the log but will not throw an error. Even if a custom file is requested.

{{% notice style="note" %}}

There are some extra steps required to get a Pi5 and an Argon ONE V3 to boot correctly.

You need to change the EEPROM settings. The setting `PSU_MAX_CURRENT=5000` is required.

Also ensure you have `usb_max_current_enable=1` in your `config.txt`

{{% /notice %}}

## Final word on configuration

If you are an advanced user and need to easily set multiple configurations it is recommended not to use build flags to disable/force default features that can be set with flags.  As these features will be unavailable to switch on without recompile.

An example of this would be the **DISABLE_POWERBUTTON** flag if you build with this option enabled it will no longer be possible to use the power button.

If you make a typo in the configuration file you will get warnings in log.  This will also set the **EF_CONF** flag.  Likewise a mistake on the command line arguments will set the *EF_ARG** flag.