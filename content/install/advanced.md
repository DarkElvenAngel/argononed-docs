+++
archetype = "chapter"
title="Advanced Build Options"
weight=1
+++

 Advanced Build options are used with `configure` or `package.sh`. Each of these options has a flag counterpart that can be set in config.txt, the configuration file, or a command line argument.

 *Note that once these are set there is no way to unset them unless you recompile.*

 **DISABLE_POWERBUTTON** *or* **FLAG 0x01** if you don't have `/dev/gpiochip0` or you don't want to use the power button then use this flag.  Remember that the Force shutdown >= 5 second long press will still work.

 **RUN_IN_FOREGROUND** *or* **FLAG 0x02** if you need the daemon to always run in the foreground this flag will skip the forking to the background and cause the daemon to log to the console.

 **USE_SYSFS_TEMP** *or* **FLAG 0x04** If your system doesn't have `/dev/vcio` you'll need to use the sysfs temperature sensor set.
