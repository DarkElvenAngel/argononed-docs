+++
archetype = "chapter"
title="How To Install"
weight=1
+++

{{% notice style="note" %}}
If you have any previous installs for your Argon you should remove them first or start with a clean install.
{{% /notice %}}

Firstly you need to have a build environment setup, that includes the following `gcc dtc git bash linux-headers make git`

{{% notice style="tip" %}}
*The package names will be different depending on your OS so I've only given their binary names. Refer to your distribution for what you need to install.*  
{{% /notice %}}
I've tried to make the installer as simple as possible. After cloning this repo simply run ```./install``` You may need to reboot for full functionality.